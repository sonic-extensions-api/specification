Sonic Extensions API Specification
==================================

This repository contains the specification for the Sonic Extensions API. This
specification is a collaboration between some of the major servers and clients
in the Subsonic ecosystem.

The specification is contained in the ``specification`` directory. If you would
like to contribute to the specification, then please read the
``CONTRIBUTING.rst`` file.
