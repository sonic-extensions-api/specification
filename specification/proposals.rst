.. |apispec| replace:: Sonic Extensions API Specification

.. sectnum::
   :start: 2

Proposals for Changes to the |apispec|
======================================

This document describes the process for submitting a change to the |apispec|.
Most changes to the specification must go through the formal RFC process. Minor
changes such as fixing grammar or spelling in the specification do not require
going through the full RFC process.

The https://gitlab.com/sonic-extensions-api/specification repository contains
the specification and issues and merge requests should be submitted there.

Additionally, anyone can create an issue to open a discussion or add an informal
proposal regarding the specification.

RFC Process
-----------

There are two high-level steps to the process: creating a proposal and applying
that proposal to the specification.

1. **Create an issue and/or merge request** (optional). It is recommended that
   you open an issue to discuss the proposal before spending a bunch of time
   writing up an actual proposal. Additionally, you can create a WIP merge
   request to gather feedback as soon as possible.

2. **Fork the repository and open a proposal MR to this repository.** The MR
   must contain:

   * The actual text of the proposal must exist in ``proposal`` directory.
   * There is a proposal template in the ``proposal`` directory.
   * The following must be addressed in the proposal document:

     * **Proposal name.** A very short phrase/sentence describing the proposal.

     * **Proposal description.** Explain the proposal in detail.

     * **Backwards compatibility considerations.** Explain considerations that
       the proposal has taken into account to ensure that the proposal will be
       backwards-compatible with the existing Subsonic API and the existing
       |apispec|.

     * **Forward compatibility considerations.** Explain considerations made in
       the proposal to reduce the likelihood that the proposal will conflict
       with future proposals to the |apispec| or the Subsonic API.

     * **Security considerations.** Provide reasonable documentation that the
       proposal does not introduce any security vulnerabilities.

     * **Potential issues.** A discussion of potential issues that the proposal
       may cause.

     * **Alternative solutions.** A discussion of alternative solutions for
       accomplishing the goals of the proposal.

     Note that these are not necessarily applicable to every single proposal.
     Use discretion as to whether or not the proposal requires each of these
     sections to be fleshed out. A brief explanation of why a specific section
     is not necessary for a given proposal will likely reduce the number of
     questions from people during the feedback period.

   * In the MR description, link to the rendered version of the proposal on
     GitLab.

3. **Gather feedback.** There will be at least 7 days for gathering feedback.
   Anyone can submit feedback on the MR.

   .. TODO create a Matrix room for discussing spec changes.

4. **Get approval of the proposal MR.** At this point, all proposals must be
   approved by Sumner Evans (`@sumner`_) who is acting as BDFL for now.

   .. note::

      As of right now, I (`@sumner`_) will consult with maintainers of the
      following server implementations before approving a proposal: Airsonic_,
      Revel_, Gonic_, and Navidrome_.

      .. _Airsonic: https://airsonic.github.io/
      .. _Revel: https://gitlab.com/robozman/revel
      .. _Gonic: https://github.com/sentriz/gonic
      .. _Navidrome: http://navidrome.org/

   .. note::

      In the future, a more formal proper governance structure may be
      implemented. However, at this point, getting the specification to a state
      that it can easily be iterated on is of utmost importance.

      Suggestions for a good governance structure are welcome, but there is no
      guarantee that any governance structure proposal will be implemented.

   .. _@sumner: https://gitlab.com/sumner

5. **Implement the proposal in at least one client and server.** (This step may
   happen asynchronously with the previous steps.) For anything to enter the
   specification, it must actually be implemented by at least one server and one
   client in the ecosystem. This will ensure that all of the extensions are
   actually practical.

6. **Submit a merge request to modify the spec.** This should be much more
   straightforward than the proposal. The main reason for this to be a separate
   step is to avoid as many annoying merge requests as possible.

7. **Get approval of the spec MR.** At this point, the MR must be approved by
   Sumner Evans (`@sumner`_).

   .. note::

      As with the proposal MRs, I (`@sumner`_) will consult with members of the
      projects listed above before accepting a spec change MR, and a more formal
      governance structure may be implemented in the future.
