.. |api| replace:: Sonic Extensions API
.. |apispec| replace:: |api| Specification

|apispec|
=========

This document describes the |api|: an open, collaborative effort to extend the
`Subsonic API`_ in a backwards-compatible manner. If you would like to
contribute to the specification, please read the :doc:`proposals` document.

.. _Subsonic API: http://www.subsonic.org/pages/api.jsp

.. sectnum::
.. contents:: Table of Contents

Introduction
------------

The existing Subsonic API defined by the Subsonic project is a client-server API
for music streaming. The Subsonic API is supported by a large ecosystem
including servers written in many different languages and targeting many
different server architectures, and clients for many different platforms. The
Subsonic API defines methods for:

* Browsing songs, artists, and albums
* Managing playlists
* User management
* And more...

However, the Subsonic API has many problems which are not being fixed in the
upstream specification (and likely will not be since Subsonic is no longer open
source software). Some of the issues with the existing specification include:

* Outdated and insecure authentication methods.
* The versioning schema is suboptimal.
* Insufficient methods for expressing the functionality that a server supports.
* There is no way of evolving the API in an open and collaborative manner.

This document describes a set of extensions to the existing Subsonic API to help
mitigate some of the problems with the existing API and to move the
specification forward in an open and collaborative manner.

Goals
~~~~~

The following *are* goals of the |api|.

* **Be an open, collaboratively maintained specification.** See the
  :doc:`proposals` document for details on how to contribute to the
  specification.

* **Be secure.*** All of the extensions must be proven to be secure.

* **Be entirely backwards-compatible with the existing Subsonic API.** Clients
  which use the |api| should be able to connect to servers which do not
  support the |api|. Likewise, clients which do not utilize the |api| should
  be able to connect to servers which support the |api|.

* **Be piecewise optional (to the furthest extent possible).** To the furthest
  extent possible, each part of the |api| should be optional. Servers should
  be able to choose what parts of the |api| that they implement, and clients
  should be able to choose what parts of the |api| that they consume.

\* Hopefully one of the byproducts of the security of the |api| is that it
will provide more secure methods of interacting with the existing Subsonic API.

Anti-Goals
~~~~~~~~~~

The following are *not* goals of the |api|.

* **Replace or deprecate the existing Subsonic API.** The |api| is entirely
  additive. Implementations of the |api| should be also be compliant with the
  Subsonic API (or at least a well-defined subset thereof).

Appendices
----------

.. toctree::
   :maxdepth: 2

   proposals
