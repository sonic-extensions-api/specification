Contributing to the Sonic Extensions API Specification
======================================================

See the `Proposals for Changes to the Sonic Extensions API Specification
<proposals_>`_.

.. _proposals: https://sonic-extensions-api.gitlab.io/specification/proposals.html

Building the Documentation
--------------------------

Building the documentation requires Sphinx_. There are many ways to install
Sphinx including:

* Using your system's package manager.
* Using pip_.
* Using the provided Pipenv_ (and if necessary, pyenv_) with the provided
  ``Pipfile``.

.. _pip: https://pip.pypa.io/en/stable/
.. _Pipenv: https://pipenv.pypa.io/en/latest/
.. _pyenv: https://github.com/pyenv/pyenv

.. _Sphinx: https://www.sphinx-doc.org/en/master/

After installing the necessary dependencies, run ``make html`` from within the
``specification`` directory.
