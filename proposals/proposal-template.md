<!-- A very short phrase/sentence describing the proposal. -->
# Proposal Name

## Proposal Description
<!-- Explain your proposal in detail. -->

## Backwards Compatibility Considerations
<!--
Explain considerations that the proposal has taken into account to ensure that
the proposal will be backwards-compatible with the existing Subsonic API and the
existing Sonic Extensions API.
-->

## Forward Compatibility Considerations
<!--
Explain considerations made in the proposal to reduce the likelihood that the
proposal will conflict with future proposals to the Sonic Extensions API or
the Subsonic API.
-->

## Security Considerations
<!--
Provide reasonable documentation that the proposal does not introduce any
security vulnerabilities.
-->

## Potential Issues
<!--
A discussion of potential issues that the proposal may cause.
-->

## Alternative Solutions
<!--
A discussion of alternative solutions for accomplishing the goals of the
proposal.
-->
